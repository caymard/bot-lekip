from datetime import datetime, timedelta
import json
import logging
from os.path import exists
from pprint import pprint
import sys

import requests


HEIGHT_THRESHOLD = 50


def date_range(start, end):
    delta = end - start
    days = [start + timedelta(days=i) for i in range(delta.days + 1)]
    return days


def analyze_image(url: str):
    payload = {
        "url": url,
        "apikey": "K83419171288957",
        "language": "fre",
        "isOverlayRequired": True,
    }
    r = requests.post(
        url="https://api.ocr.space/parse/image",
        data=payload,
    )
    return r.json()


def load_data_from_file(path):
    with open(path, "r") as f:
        return json.load(f)


def compute_text_size(data):
    res = []
    for line in data.get("ParsedResults")[0].get("TextOverlay").get("Lines"):
        res.append(
            {
                "text": line.get("LineText"),
                "height": max([word.get("Height") for word in line.get("Words")]),
            }
        )
    return res


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    query = sys.argv[1]
    if len(sys.argv) == 2:
        queries = [sys.argv[1]]
    elif len(sys.argv) == 3:
        sd = [int(i) for i in sys.argv[1].split("-")]
        ed = [int(i) for i in sys.argv[2].split("-")]
        start_date = datetime(sd[0], sd[1], sd[2])
        end_date = datetime(ed[0], ed[1], ed[2])
        queries = [
            date.strftime("%Y-%m-%d") for date in date_range(start_date, end_date)
        ]
    else:
        sys.exit("Wrong arguments")
    for query in queries:
        data_path = "./data/" + query + ".json"
        if not exists(data_path):
            logging.info("No data for {}, running analyze".format(query))
            data = analyze_image(
                "https://cdn.artphotolimited.com/unes/l-equipe/1000x1000/{}.jpg".format(
                    query
                )
            )
            if type(data) is str:
                logging.error("Error from api: {}".format(data))
                continue
            with open(data_path, "w") as f:
                json.dump(data, f)
        else:
            logging.info("Data already exists for {}".format(query))
