import elasticsearch
import sys
from pprint import pprint

HEIGHT_THRESHOLD = 50


def usage():
    sys.exit("Usage: python " + sys.argv[0] + " word")


# check command line
if len(sys.argv) != 2:
    print(str(len(sys.argv)) + " args provided")
    usage()


word = sys.argv[1]

es = elasticsearch.Elasticsearch(
    hosts=["http://db:9200"]
)  # use default of localhost, port 9200


query = {"match": {"text": {"query": word, "fuzziness": "AUTO"}}}

results = es.search(query=query)
# pprint(results)

for hit in results.get("hits").get("hits"):
    print("{} : {}".format(hit["_source"]["journal"], hit["_source"]["text"]))
