import logging
from lxml import html
import requests
from datetime import datetime


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    now = datetime.now()
    img_src = "https://cdn.artphotolimited.com/unes/l-equipe/1000x1000/{}.jpg".format(
        now.strftime("%Y-%m-%d")
    )
    image = requests.get(img_src)
    logging.info("Image downloaded: %s" % img_src)

    with open("une.jpg", "wb") as f:
        f.write(image.content)
        f.close()

    logging.info("Une saved in file une.jpg, bye.")
