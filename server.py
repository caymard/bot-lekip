from os import environ as env

import elasticsearch
from flask import Flask, render_template, request
import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration

sentry_sdk.init(
    dsn=env.get("SENTRY_DSN"),
    integrations=[FlaskIntegration()],
    traces_sample_rate=1.0,
)

app = Flask(__name__)

base_url = "https://cdn.artphotolimited.com/unes/l-equipe/1000x1000/{}.jpg"


@app.route("/ping", methods=["GET"])
def ping():
    return "pong"


@app.route("/", methods=["GET"])
def hello():
    word = request.args.get("word")
    if word is None:
        return render_template("index.html")

    with sentry_sdk.start_span(op="es", description="search") as span:
        es = elasticsearch.Elasticsearch(
            hosts=["http://{}:9200".format(env.get("ELASTICSEARCH_HOST", "localhost"))]
        )
        query = {"match": {"text": {"query": word, "fuzziness": "AUTO"}}}
        results = es.search(query=query)
        span.set_data("es.hits", len(results.get("hits").get("hits")))
    images = [
        base_url.format(hit["_source"]["journal"])
        for hit in results.get("hits").get("hits")
    ]
    print(images)
    return render_template("index.html", word=word, images=images)


if __name__ == "__main__":
    app.run(debug=True)
