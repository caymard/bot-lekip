FROM python:3.10-slim

RUN useradd -d /app -m app
WORKDIR /app
USER app
ENV PATH=/app/.local/bin:$PATH

RUN pip install -qU pip
ADD requirements.txt .
RUN pip install -qUr requirements.txt

ADD *.py ./
ADD templates/ ./templates/

CMD ["gunicorn", "--bind", "0.0.0.0:5000", "--workers=2", "--threads=2", "server:app"]
