from datetime import datetime, timedelta
import elasticsearch
import hashlib
import sys
import os
import json

HEIGHT_THRESHOLD = 50


def date_range(start, end):
    delta = end - start
    days = [start + timedelta(days=i) for i in range(delta.days + 1)]
    return days


def usage():
    helpmessage = [
        "Usage: python " + sys.argv[0] + " journal_date",
        "       python " + sys.argv[0] + " start_date end_date",
    ]
    sys.exit("\n".join(helpmessage))


def compute_text_size(data):
    res = []
    for line in data.get("ParsedResults")[0].get("TextOverlay").get("Lines"):
        res.append(
            {
                "text": line.get("LineText"),
                "height": max([word.get("Height") for word in line.get("Words")]),
            }
        )
    return res


def load_data_from_file(path):
    with open(path, "r") as f:
        return json.load(f)


# check command line
if len(sys.argv) < 2 or 3 < len(sys.argv):
    print(str(len(sys.argv)) + " args provided")
    usage()

es = elasticsearch.Elasticsearch(hosts=["http://db:9200"])

# get journal dates
if len(sys.argv) == 2:
    journal_dates = [sys.argv[1]]
elif len(sys.argv) == 3:
    sd = [int(i) for i in sys.argv[1].split("-")]
    ed = [int(i) for i in sys.argv[2].split("-")]
    start_date = datetime(sd[0], sd[1], sd[2])
    end_date = datetime(ed[0], ed[1], ed[2])
    journal_dates = [
        date.strftime("%Y-%m-%d") for date in date_range(start_date, end_date)
    ]

for journal_date in journal_dates:
    journal_date_path = "./data/" + journal_date + ".json"
    # check file exists
    if os.path.isfile(journal_date_path) is False:
        print("File not found: " + journal_date_path)
        continue

    # load data
    journal_data = load_data_from_file(journal_date_path)
    if type(journal_data) is str:
        print("Data is a string")
        continue
    # check computation errors
    try:
        if journal_data.get("IsErroredOnProcessing"):
            print("ERROR: no data for {}".format(journal_date))
            continue
    except AttributeError:
        import pdb

        pdb.set_trace()
    lines = compute_text_size(journal_data)
    lines_by_height = sorted(lines, key=lambda d: d["height"])
    filtered_lines_by_height = [
        line for line in lines_by_height if line["height"] > HEIGHT_THRESHOLD
    ]
    # read each line
    for txtNum, line in enumerate(filtered_lines_by_height):
        line_id = hashlib.md5(
            (journal_date + line.get("text")).encode("utf-8")
        ).hexdigest()
        es.index(
            index="main",
            id=line_id,
            document={
                "height": line.get("height"),
                "text": line.get("text"),
                "journal": journal_date,
            },
        )
        # print(es.get(index="main", id=line_id))
