# BOT LEKIP

This repository has 2 projects in 1:

- daily fetch front page of sports french newspaper L'Equipe and send it by mail
- web service to get old front pages by typing a word

## Daily fetch

`run.py`: download the front page.

`send_email.py`: send the front page by email.

## Web service

`reader.py`: extract words from front pages and store them in JSON files.

`load_db.py`: load database with JSON files.

`server.py`: run the server.

The best way to have the server up and running is to use Docker of course:

```bash
docker-compose build
docker-compose pull
docker-compose up -d
```
