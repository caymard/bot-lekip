from datetime import datetime
from email import encoders
from email.header import Header
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import os
import smtplib

if __name__ == "__main__":
    # get user input
    # input sender email address and password:
    from_addr = "caymard.bot@gmail.com"
    password = os.environ.get("CAYMARD_BOT_PASSWORD")
    # input receiver email address.
    to_addr = "aymard.clement@gmail.com"
    # input smtp server ip address:
    smtp_server = "smtp.gmail.com"
    # email object that has multiple part:
    msg = MIMEMultipart()
    msg["From"] = "BOT LEKIP"
    msg["To"] = to_addr
    now = datetime.now()
    msg["Subject"] = Header(
        "La Une de L'Equipe du {}".format(now.strftime("%d/%m/%Y")), "utf-8"
    ).encode()
    # attache a MIMEText object to save email content
    msg_content = MIMEText(
        "<a href='https://lequipe.fr'><img src='cid:image1'></a>", "html"
    )
    msg.attach(msg_content)
    # to add an attachment is just add a MIMEBase object to read a picture locally.
    with open("une.jpg", "rb") as f:
        image = MIMEImage(f.read())
        image.add_header("Content-ID", "<image1>")
        msg.attach(image)
    server = smtplib.SMTP_SSL(smtp_server, 465)
    server.login(from_addr, password)
    server.sendmail(from_addr, [to_addr], msg.as_string())
    server.quit()
